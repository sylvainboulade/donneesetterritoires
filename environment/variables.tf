variable "base_domain" {
  type = string
}
variable "grist_form_base_domain" {
  type = string
}

variable "old_project_slug" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}
locals {
  environment_slug = var.gitlab_environment_scope == "*" ? "review" : var.gitlab_environment_scope
}

variable "namespace" {
  type = string
}

variable "gitlab_project_ids" {
  type = object({
    zonage     = number
    grist_form = number
    umap       = number
  })
}

variable "namespace_quota_max_cpu" {
  type    = number
  default = 2
}

variable "namespace_quota_max_memory" {
  type    = string
  default = "12Gi"
}

variable "monitoring_org_id" {
  type = string
}

variable "grist_form_grist_url" {
  type    = string
  default = ""
}
variable "grist_form_db_path" {
  type      = string
  default   = ""
  sensitive = true
}
variable "grist_form_grist_api_token" {
  type      = string
  default   = ""
  sensitive = true
}
