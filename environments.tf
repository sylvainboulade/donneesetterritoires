module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = var.dev_base_domain
  grist_form_base_domain   = "formulaires.${var.dev_base_domain}"
  namespace                = "${var.old_project_slug}-development"
  old_project_slug         = var.old_project_slug
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 8
  namespace_quota_max_memory = "12Gi"

  gitlab_project_ids = local.gitlab_project_ids
  monitoring_org_id  = random_string.development_secret_org_id.result

}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base_domain              = var.prod_base_domain
  namespace                = var.old_project_slug
  old_project_slug         = var.old_project_slug
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 8
  namespace_quota_max_memory = "12Gi"

  gitlab_project_ids = local.gitlab_project_ids
  monitoring_org_id  = random_string.production_secret_org_id.result

  grist_form_base_domain     = var.production_grist_form_base_domain
  grist_form_db_path         = var.production_grist_form_db_path
  grist_form_grist_api_token = var.production_grist_form_grist_api_token
  grist_form_grist_url       = "https://grist.incubateur.anct.gouv.fr/"
}

resource "scaleway_object_bucket" "umap_dev_backups" {
  provider = scaleway.project
  name     = "${var.project_slug}-umap-dev-backups"
}
module "umap_development" {
  source                   = "./environment_umap"
  gitlab_environment_scope = "development"
  project_id               = local.gitlab_project_ids.umap
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = "umap.dev.incubateur.anct.gouv.fr"
  namespace                = "${var.project_slug}-umap"
  backup_bucket_name       = scaleway_object_bucket.umap_dev_backups.name
  backup_bucket_access_key = var.scaleway_project_config.access_key
  backup_bucket_secret_key = var.scaleway_project_config.secret_key
  backup_bucket_endpoint   = "https://s3.fr-par.scw.cloud"
  backup_bucket_region     = "fr-par"
  sso_endpoint             = var.development_tools_umap_sso_endpoint
  sso_key                  = var.development_tools_umap_sso_key
  sso_secret               = var.development_tools_umap_sso_secret
  monitoring_org_id        = random_string.development_secret_org_id.result

  providers = {
    helm       = helm.development
    kubernetes = kubernetes.development
  }
}

resource "scaleway_object_bucket" "umap_prod_backups" {
  provider = scaleway.project
  name     = "${var.project_slug}-umap-prod-backups"
}
module "umap_production" {
  source                   = "./environment_umap"
  gitlab_environment_scope = "production"
  project_id               = local.gitlab_project_ids.umap
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base_domain              = "umap.incubateur.anct.gouv.fr"
  namespace                = "${var.project_slug}-umap"
  backup_bucket_name       = scaleway_object_bucket.umap_dev_backups.name
  backup_bucket_access_key = var.scaleway_project_config.access_key
  backup_bucket_secret_key = var.scaleway_project_config.secret_key
  backup_bucket_endpoint   = "https://s3.fr-par.scw.cloud"
  backup_bucket_region     = "fr-par"
  sso_endpoint             = var.production_tools_umap_sso_endpoint
  sso_key                  = var.production_tools_umap_sso_key
  sso_secret               = var.production_tools_umap_sso_secret
  monitoring_org_id        = random_string.production_secret_org_id.result

  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
  }
}

module "catalog_development" {
  source                   = "./environment_catalog"
  gitlab_environment_scope = "development"
  project_id               = local.gitlab_project_ids.catalog
  base_domain              = "catalogue-indicateurs.${var.dev_base_domain}"
  namespace                = "${var.project_slug}-catalog-development"
  monitoring_org_id        = random_string.development_secret_org_id.result
  kubeconfig               = local.kubeconfig_development

  providers = {
    helm       = helm.development
    kubernetes = kubernetes.development
  }
}

module "catalog_production" {
  source                   = "./environment_catalog"
  gitlab_environment_scope = "production"
  project_id               = local.gitlab_project_ids.catalog
  base_domain              = "catalogue-indicateurs.${var.prod_base_domain}"
  namespace                = "${var.project_slug}-catalog"
  monitoring_org_id        = random_string.production_secret_org_id.result
  kubeconfig               = local.kubeconfig_production

  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
  }
}
