locals {
  grist_release_name = "grist"
  grist_typeorm_config = indent(2, <<-EOT
      TYPEORM_TYPE: postgres
      TYPEORM_DATABASE:
        secretKeyRef:
          key: dbname
          name: "${module.postgresql.secret-name}"
      TYPEORM_HOST:
        secretKeyRef:
          key: host
          name: "${module.postgresql.secret-name}"
      TYPEORM_USERNAME:
        secretKeyRef:
          key: user
          name: "${module.postgresql.secret-name}"
      TYPEORM_PASSWORD:
        secretKeyRef:
          key: password
          name: "${module.postgresql.secret-name}"
      TYPEORM_EXTRA: '{"ssl": true, "extra": {"ssl": {"rejectUnauthorized": false}}}'
      EOT
  )
  grist_custom_script_env = var.custom_script == null ? "" : yamlencode({
    GRIST_INCLUDE_CUSTOM_SCRIPT_URL = element(split("/", var.custom_script.path), length(split("/", var.custom_script.path)) - 1)
  })
}

resource "scaleway_object_bucket" "grist_backups" {
  name = "${var.project_slug}-backups"
}

resource "scaleway_object_bucket" "grist_snapshots" {
  name = "${var.project_slug}-snapshots"
  versioning {
    enabled = true
  }
}

resource "random_password" "redis_password" {
  length  = 32
  special = false
}

module "redis" {
  source        = "gitlab.com/vigigloo/tools-k8s/redis"
  version       = "0.1.1"
  namespace     = module.namespace.namespace
  chart_name    = "redis"
  chart_version = "17.4.2"

  redis_password = resource.random_password.redis_password.result
  redis_replicas = 0
}

module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = "4"
  max_memory_limits = local.namespace_limits_memory
  namespace         = var.override_namespace != null ? var.override_namespace : var.project_slug
  project_name      = "Grist"
  project_slug      = var.override_namespace != null ? var.override_namespace : var.project_slug

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}
moved {
  from = module.grist_namespace
  to   = module.namespace
}

resource "random_password" "grist_boot_key" {
  length  = 128
  special = false
}

module "grist" {
  source        = "gitlab.com/vigigloo/tools-k8s/grist"
  version       = "1.1.1"
  namespace     = module.namespace.namespace
  chart_name    = local.grist_release_name
  chart_version = "2.0.0"
  values = [
    <<-EOT
    envVars:
      GRIST_BOOT_KEY: ${random_password.grist_boot_key.result}
      APP_HOME_URL: https://${var.domain}
      GRIST_ORG_IN_PATH: "true"
      GRIST_DEFAULT_EMAIL: "${var.default_email}"
      GRIST_SANDBOX_FLAVOR: gvisor
      APP_STATIC_INCLUDE_CUSTOM_CSS: true
      ${local.grist_custom_script_env}
      GRIST_DEFAULT_LOCALE: fr
      GRIST_HELP_CENTER: https://au-carre.gitbook.io/doc-grist/creer-un-document
      FREE_COACHING_CALL_URL: mailto:donnees@anct.gouv.fr
      GRIST_CONTACT_SUPPORT_URL: mailto:donnees@anct.gouv.fr
      GRIST_ANON_PLAYGROUND: false
      GRIST_DOCS_MINIO_BUCKET: ${scaleway_object_bucket.grist_snapshots.name}
      GRIST_DOCS_MINIO_ENDPOINT: s3.fr-par.scw.cloud
      GRIST_DOCS_MINIO_BUCKET_REGION: fr-par
      GRIST_PROMCLIENT_PORT: "9102"
      GRIST_HIDE_UI_ELEMENTS: "billing,sendToDrive,createSite"
      GRIST_ALLOWED_HOSTS: ${var.domain}
      PERMITTED_CUSTOM_WIDGETS: "calendar"
      GVISOR_LIMIT_MEMORY: "${local.grist_gvisor_limit_memory}"
      GRIST_OIDC_IDP_ISSUER: "https://${var.oauth_domain}/.well-known/openid-configuration"
      GRIST_OIDC_IDP_CLIENT_ID: "${var.oauth_client_id}"
      GRIST_OIDC_IDP_SCOPES: "${var.oauth_scopes}"
      GRIST_SKIP_REDIS_CHECKSUM_MISMATCH: true
      ${local.grist_typeorm_config}
      ${length(var.grist_extra_env) > 0 ? indent(2, yamlencode(var.grist_extra_env)) : ""}
    backup:
      image:
        tag: a61a5e4ac365618cadf6d69609c77b7facde8763
    command: ["/bin/sh","-c"]
    args: ["cd /grist && export NODE_OPTIONS='--max-old-space-size=${local.grist_max_old_space_size}' && ./sandbox/run.sh"]
    ingress:
      enabled: true
      host: ${var.domain}
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
        haproxy.org/cors-enable: "true"
        haproxy.org/cors-allow-origin: "${var.cors_allow_origin}"
        haproxy.org/cors-allow-methods: "GET"
        haproxy.org/cors-allow-headers: "*"
        haproxy.org/response-set-header: X-Robots-Tag none
    EOT
    , <<-EOT
    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
      prometheus.io/scrape: "true"
      prometheus.io/path: "/"
      prometheus.io/port: "9102"
    envVars:
      GRIST_DOCS_MINIO_ACCESS_KEY: ${var.scaleway_project_config.access_key}
      GRIST_DOCS_MINIO_SECRET_KEY: ${var.scaleway_project_config.secret_key}
      REDIS_URL: redis://default:${resource.random_password.redis_password.result}@${module.redis.hostname}
      GRIST_OIDC_IDP_CLIENT_SECRET: "${var.oauth_client_secret}"
    EOT
  ]
  limits_memory = local.grist_container_limits_memory
  requests_cpu  = "200m"

  backup_limits_memory = "600Mi"
  backup_requests_cpu  = "100m"

  grist_persistence_size     = var.grist_persistence_size
  grist_backup_schedule      = var.backup_schedule
  grist_backup_s3_endpoint   = "https://s3.fr-par.scw.cloud"
  grist_backup_s3_region     = scaleway_object_bucket.grist_backups.region
  grist_backup_s3_bucket     = scaleway_object_bucket.grist_backups.name
  grist_backup_s3_access_key = var.scaleway_project_config.access_key
  grist_backup_s3_secret_key = var.scaleway_project_config.secret_key

  image_repository = var.image_repository
  image_tag        = var.image_tag

  grist_mount_files = concat([
    {
      path    = "/grist/static/ui-icons/Logo/logo_anct_2022.svg"
      content = filebase64("${path.module}/logo_anct_2022.svg")
    },
    {
      path    = "/grist/static/custom.css"
      content = filebase64("${path.module}/custom.css")
    },
    ],
    var.custom_script == null ? [] : [var.custom_script]
  )
}

moved {
  from = helm_release.grist
  to   = module.grist.helm_release.grist
}
