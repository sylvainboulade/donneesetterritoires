variable "project_slug" {
  type = string
}
variable "override_namespace" {
  type    = string
  default = null
}

variable "domain" {
  type = string
}

variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}

variable "oauth_domain" {
  type = string
}

variable "oauth_client_id" {
  type = string
}

variable "oauth_client_secret" {
  type      = string
  sensitive = true
}
variable "oauth_scopes" {
  type    = string
  default = "openid email profile organization"
}

variable "default_email" {
  type = string
}

variable "monitoring_org_id" {
  type      = string
  sensitive = true
}

variable "kubeconfig" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
  default = null
}

variable "generate_kubeconfigs" {
  type    = set(string)
  default = []
}

variable "grist_limits_memory_mb" {
  type = number
}
locals {
  namespace_limits_memory       = "${var.grist_limits_memory_mb + 6 * 1024}Mi"
  grist_container_limits_memory = "${var.grist_limits_memory_mb}Mi"
  grist_max_old_space_size      = floor(var.grist_limits_memory_mb * 0.45)
  grist_gvisor_limit_memory     = floor(var.grist_limits_memory_mb * 0.45 * 1024 * 1024)
}

variable "grist_doc_wk_replicas" {
  type        = number
  default     = 0
  description = "Unused, only there to easily compare modules"
}
variable "grist_doc_wk_requests_cpu_m" {
  type        = number
  default     = 0
  description = "Unused, only there to easily compare modules"
}
variable "grist_doc_wk_limits_memory_mb" {
  type        = number
  default     = 0
  description = "Unused, only there to easily compare modules"
}
variable "grist_home_wk_replicas" {
  type        = number
  default     = 0
  description = "Unused, only there to easily compare modules"
}
variable "grist_home_wk_requests_cpu_m" {
  type        = number
  default     = 0
  description = "Unused, only there to easily compare modules"
}
variable "grist_home_wk_limits_memory_mb" {
  type        = number
  default     = 0
  description = "Unused, only there to easily compare modules"
}

variable "grist_persistence_size" {
  type    = string
  default = "10Gi"
}

variable "backup_schedule" {
  type    = string
  default = "0 1 * * *"
}

variable "grist_extra_env" {
  type    = map(string)
  default = {}
}
variable "cors_allow_origin" {
  type = string
}

variable "image_repository" {
  type = string
}
variable "image_tag" {
  type = string
}

variable "database_volume_size" {
  type    = string
  default = "10Gi"
}

variable "custom_script" {
  type = object({
    path    = string
    content = string
  })
  default = null
}
