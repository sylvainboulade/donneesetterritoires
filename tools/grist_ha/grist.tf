locals {
  grist_release_name = "grist"
  grist_namespace    = var.override_namespace != null ? var.override_namespace : var.project_slug
  grist_typeorm_config = indent(2, <<-EOT
      TYPEORM_TYPE: postgres
      TYPEORM_DATABASE:
        secretKeyRef:
          key: dbname
          name: "${module.postgresql.secret-name}"
      TYPEORM_HOST:
        secretKeyRef:
          key: host
          name: "${module.postgresql.secret-name}"
      TYPEORM_USERNAME:
        secretKeyRef:
          key: user
          name: "${module.postgresql.secret-name}"
      TYPEORM_PASSWORD:
        secretKeyRef:
          key: password
          name: "${module.postgresql.secret-name}"
      TYPEORM_EXTRA: '{"ssl": true, "extra": {"ssl": {"rejectUnauthorized": false}}}'
      EOT
  )
  grist_custom_script_env = var.custom_script == null ? "" : yamlencode({
    GRIST_INCLUDE_CUSTOM_SCRIPT_URL = element(split("/", var.custom_script.path), length(split("/", var.custom_script.path)) - 1)
  })
  grist_custom_script_yaml = var.custom_script == null ? "" : yamlencode(
    [
      {
        path    = var.custom_script.path,
        content = var.custom_script.content,
      },
    ]
  )
}

resource "scaleway_object_bucket" "grist_backups" {
  name = "${var.project_slug}-backups"
}

resource "scaleway_object_bucket" "grist_snapshots" {
  name = "${var.project_slug}-snapshots"
  versioning {
    enabled = true
  }
}

resource "random_password" "redis_password" {
  length  = 32
  special = false
}

module "redis" {
  source        = "gitlab.com/vigigloo/tools-k8s/redis"
  version       = "0.1.1"
  namespace     = module.namespace.namespace
  chart_name    = "redis"
  chart_version = "17.4.2"

  redis_password = resource.random_password.redis_password.result
  redis_replicas = 0
}

module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = local.namespace_requests_cpu
  max_memory_limits = local.namespace_limits_memory
  namespace         = var.override_namespace != null ? var.override_namespace : var.project_slug
  project_name      = "Grist"
  project_slug      = var.override_namespace != null ? var.override_namespace : var.project_slug

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

resource "random_password" "grist_boot_key" {
  length  = 128
  special = false
}

resource "helm_release" "grist" {
  repository = "https://numerique-gouv.github.io/helm-charts/"
  chart      = "grist"
  version    = "5.3.2"
  name       = local.grist_release_name
  namespace  = local.grist_namespace
  timeout    = 600
  values = [
    <<-EOT
    image:
      repository: ${var.image_repository}
      tag: ${var.image_tag}

    commonEnvVars: &commonEnvVars
      GRIST_BOOT_KEY: ${random_password.grist_boot_key.result}
      APP_HOME_URL: https://${var.domain}
      GRIST_ORG_IN_PATH: "true"
      GRIST_DEFAULT_EMAIL: "${var.default_email}"
      GRIST_SANDBOX_FLAVOR: gvisor
      APP_STATIC_INCLUDE_CUSTOM_CSS: true
      ${local.grist_custom_script_env}
      GRIST_DEFAULT_LOCALE: fr
      GRIST_HELP_CENTER: https://au-carre.gitbook.io/doc-grist/creer-un-document
      FREE_COACHING_CALL_URL: mailto:donnees@anct.gouv.fr
      GRIST_CONTACT_SUPPORT_URL: mailto:donnees@anct.gouv.fr
      GRIST_ANON_PLAYGROUND: false
      PERMITTED_CUSTOM_WIDGETS: "calendar"
      GRIST_PROMCLIENT_PORT: "9102"
      GRIST_HIDE_UI_ELEMENTS: "billing,sendToDrive,createSite"
      GRIST_MANAGED_WORKERS: true
      GRIST_SKIP_REDIS_CHECKSUM_MISMATCH: true
      ${length(var.grist_extra_env) > 0 ? indent(2, yamlencode(var.grist_extra_env)) : ""}

      GRIST_ALLOWED_HOSTS: ${var.domain}
      GRIST_LOG_SKIP_HTTP: ""
      GRIST_SINGLE_PORT: 0

      ${local.grist_typeorm_config}
      REDIS_URL: redis://default:${resource.random_password.redis_password.result}@${module.redis.hostname}

      GRIST_OIDC_IDP_ISSUER: "https://${var.oauth_domain}/.well-known/openid-configuration"
      GRIST_OIDC_IDP_CLIENT_ID: "${var.oauth_client_id}"
      GRIST_OIDC_IDP_CLIENT_SECRET: "${var.oauth_client_secret}"
      GRIST_OIDC_IDP_SCOPES: "${var.oauth_scopes}"

    commonPodAnnotations: &commonPodAnnotations
      monitoring-org-id: ${var.monitoring_org_id}
      prometheus.io/scrape: "true"
      prometheus.io/path: "/"
      prometheus.io/port: "9102"

    mountFiles:
      - path: "/grist/static/ui-icons/Logo/logo_anct_2022.svg"
        content: ${filebase64("${path.module}/logo_anct_2022.svg")}
      - path: "/grist/static/custom.css"
        content: ${filebase64("${path.module}/custom.css")}
      ${indent(2, local.grist_custom_script_yaml)}

    docWorker:
      replicas: ${var.grist_doc_wk_replicas}
      envVars:
        <<: *commonEnvVars

        GRIST_SERVERS: docs

        GRIST_DOCS_MINIO_BUCKET: ${scaleway_object_bucket.grist_snapshots.name}
        GRIST_DOCS_MINIO_ENDPOINT: s3.fr-par.scw.cloud
        GRIST_DOCS_MINIO_BUCKET_REGION: fr-par
        GRIST_DOCS_MINIO_ACCESS_KEY: ${var.scaleway_project_config.access_key}
        GRIST_DOCS_MINIO_SECRET_KEY: ${var.scaleway_project_config.secret_key}

      podAnnotations:
        <<: *commonPodAnnotations

      resources:
        limits:
          memory: ${local.grist_doc_wk_container_limits_memory}
        requests:
          cpu: ${local.grist_doc_wk_container_requests_cpu}

      persistence:
        core-dump:
          type: emptyDir
          mountPath: /tmp/core_dumps

      shareProcessNamespace: false

      probes:
        liveness:
          exec:
            command:
              - node
              - -e
              - |
                fetch('http://127.0.0.1:8484/status?db=1&redis=1')
                  .then(resp => {
                    if (resp.ok === false) { process.exit(1); }
                  })
                  .then(() => {
                    const e = process.env;
                    const c = require('redis').createClient(e.REDIS_URL);
                    const dwid = e.POD_IP.replaceAll('.', '-');
                    c.on('connect',
                      () => c.sismember('workers-available', dwid, (err, v) => {
                        process.exit(err || v ? 0 : 1)
                      })
                    )
                  });

    homeWorker:
      replicas: ${var.grist_home_wk_replicas}
      envVars:
        <<: *commonEnvVars

        GRIST_SERVERS: home,static

      podAnnotations:
        <<: *commonPodAnnotations

      resources:
        limits:
          memory: ${local.grist_home_wk_container_limits_memory}
        requests:
          cpu: ${local.grist_home_wk_container_requests_cpu}

      persistence:
        core-dump:
          type: emptyDir
          mountPath: /tmp/core_dumps

      shareProcessNamespace: false

    loadBalancer:
      replicas: 2

    ingress:
      enabled: true
      host: ${var.domain}
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
        haproxy.org/cors-enable: "true"
        haproxy.org/cors-allow-origin: "${var.cors_allow_origin}"
        haproxy.org/cors-allow-methods: "GET"
        haproxy.org/cors-allow-headers: "*"
        haproxy.org/response-set-header: X-Robots-Tag none
    EOT
  ]
}

moved {
  from = module.grist_namespace
  to   = module.namespace
}
moved {
  from = module.grist.helm_release.grist
  to   = helm_release.grist
}
